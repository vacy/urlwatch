package lib;

import org.apache.commons.text.similarity.JaroWinklerSimilarity;
import org.junit.jupiter.api.Test;
import vacyit.lib.Cache;
import vacyit.lib.GetChecksum;
import vacyit.lib.Webrequest;
import vacyit.lib.Watchitem;
import java.net.URI;
import java.net.URISyntaxException;
import com.google.common.base.Stopwatch;
import java.util.Scanner;
import java.util.Timer;

import static org.junit.jupiter.api.Assertions.*;

class WebrequestTest {
    @Test
    public void WebrequestAndHash(){
        String newhash = null;

        try {
            newhash = GetChecksum.hash(Webrequest.get(new URI("http://info.cern.ch/hypertext/WWW/TheProject.html")));
        }
        catch(URISyntaxException e) {
            e.printStackTrace();
        }
        assertEquals("3640321671", newhash);
    }

    @Test
    public void WebrequestAndSimilarity(){

        try {
            String content1 = Webrequest.get(new URI("http://info.cern.ch/hypertext/WWW/TheProject.html"));
            String content2 = Webrequest.get(new URI("http://info.cern.ch/hypertext/WWW/TheProject.html"));
            JaroWinklerSimilarity cmpSimilarity = new JaroWinklerSimilarity();
            Stopwatch timer = Stopwatch.createStarted();
            Double similarity = cmpSimilarity.apply(content1, content2);
            System.out.println("Method took: " + timer.stop());
            System.out.println(similarity.toString());
        }
        catch(URISyntaxException e) {
            e.printStackTrace();
        }
        assertEquals("1", "1");
    }

    @Test
    public void WebrequestAndSimilarity2(){

        try {
            URI uri = new URI("https://nextcloud.com/changelog");

            Stopwatch timer = Stopwatch.createStarted();
            String content2 = Webrequest.get(uri);
            System.out.println("Method Webrequest.get took: " + timer.stop());

            String content1 = Cache.readStream(uri);

            System.out.println("Filename hash: " + GetChecksum.hash(uri.toASCIIString()));
            timer = Stopwatch.createStarted();
            JaroWinklerSimilarity cmpSimilarity = new JaroWinklerSimilarity();
            Double similarity = cmpSimilarity.apply(content2, content2);
            System.out.println("Method Similarity took: " + timer.stop());
            System.out.println(similarity.toString());

            Watchitem wi = new Watchitem();
            wi.setURL(uri.toASCIIString());
            wi.setCache(content2);
            Cache.writeStream(wi);
        }
        catch(URISyntaxException e) {
            e.printStackTrace();
        }
        assertEquals("1", "1");
    }
}