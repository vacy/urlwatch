package lib;

import org.junit.jupiter.api.Test;
import vacyit.lib.Cache;
import vacyit.lib.GetChecksum;
import vacyit.lib.Watchitem;
import vacyit.lib.Webrequest;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import org.apache.commons.io.IOUtils;

import static org.junit.jupiter.api.Assertions.*;

class CacheTest {


    private String readFromInputStream(InputStream inputStream) throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br
                     = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }

    @Test
    void write() {


        Watchitem wi = new Watchitem();
        wi.setURL("https://dachzeltnomaden.com/dachzelt-messen-treffen/");
        String filename = "/tmp/urlwatch-" + GetChecksum.hash(wi.getURL().toASCIIString()) + ".cache";
        System.out.println(filename);
        String content = Webrequest.get(wi.getURL());
        wi.setCache(content);

        Cache.writeStream(wi);


        String contentread = Cache.readStream(wi.getURL());
        //String contentread = Cache.read(wi.getURL());
        wi.setCache(contentread);
        System.out.println(GetChecksum.similarity(content, contentread));
        try {
            Formatter fo = new Formatter(filename + "rw");
            fo.format("%s", wi.getCache());
            fo.close();
        } catch(FileNotFoundException e){ e.printStackTrace(); }
    }

    @Test
    void read() {

    }


}