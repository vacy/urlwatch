package vacyit.lib;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import vacyit.lib.Watchfile;
import vacyit.lib.Watchitem;

public class WatchfileTest {
    @Test
    public void ReadAndWriteForExistingWatchitems() {
        LinkedList<Watchitem> testlist = new LinkedList<>();
        Watchitem ti = new Watchitem();

        ti.setOperation('=');
        ti.setURL("https://web.de");
        ti.setDelimiter(',');
        testlist.add(ti);

        Watchfile watchfile = new Watchfile();
        try{
            watchfile.setPath(new URI("/tmp/testurl.watch"));
        }catch(URISyntaxException e){ e.printStackTrace(); }

        watchfile.write(testlist);

        LinkedList<Watchitem> watchlist = watchfile.read();
        Watchitem wi = watchlist.getFirst();
        // ti=testitem & wi=watchitem
        assertAll(
                () -> assertEquals(ti.getURL(), wi.getURL()),
                () -> assertEquals(ti.getOperation(), wi.getOperation()),
                () -> assertEquals(ti.getDelimiter(), wi.getDelimiter())
                );
    }
}

