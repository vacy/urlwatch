package vacyit.lib;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import vacyit.lib.GetChecksum;

class ChecksumTest {
    @Test
    void checksum() {
        assertEquals("3632233996", GetChecksum.hash("test"));
    }
}