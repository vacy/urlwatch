package vacyit.lib;

import com.google.common.base.Stopwatch;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ObjectUtils;

import java.io.File;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream
import java.io.OutputStream;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;
import java.net.URI;

public class Cache {
    /*
     *public static String read(URI url){
     *   String content = "";
     *   try {
     *       File fi = new File("/tmp/urlwatch-" + GetChecksum.hash(url.toASCIIString()) + ".cache");
     *       Scanner sc = new Scanner(fi);
     *       while(sc.hasNext()){
     *           // go in fileloop as long as content is left and pull a line
     *           content += sc.nextLine();
     *           content += "\n";
     *           //if(sc.hasNext()){ content += "\n"; }
     *       }
     *   }
     *   catch(NullPointerException e){ e.printStackTrace(); }
     *   catch(FileNotFoundException e){
     *       e.printStackTrace();
     *       System.out.println(url.toASCIIString());
     *   }
     *   finally {
     *       return content;
     *   }
     *}
    */

    public static String readStream(URI url){
        String cache = null;
        String filename = "/tmp/urlwatch-" + GetChecksum.hash(url.toASCIIString()) + ".cache";
        try{
            FileInputStream fis = new FileInputStream(filename);
            cache = IOUtils.toString(fis, "UTF-8");

        } catch(Exception e) {
            e.printStackTrace();
        }
        return cache;

    }

    /*
    *public static void write(Watchitem wi){
    *    String filename = "/tmp/urlwatch-" + GetChecksum.hash(wi.getURL().toASCIIString()) + ".cache";
    *        try {
    *            Formatter fo = new Formatter(filename);
    *            fo.format("%s", wi.getCache());
    *            fo.close();
    *        } catch(FileNotFoundException e){ e.printStackTrace(); }
    }*/

    public static void writeStream(Watchitem wi){
        String filename = "/tmp/urlwatch-" + GetChecksum.hash(wi.getURL().toASCIIString()) + ".cache";
        byte[] bytes = wi.getCache().getBytes();
        try (OutputStream out = new FileOutputStream(filename)) {
            out.write(bytes); } catch(IOException e){e.printStackTrace();}
    }

    public static void delete(URI url){
        System.out.println("deleting: " + url);
        try {
            File fi = new File("/tmp/urlwatch-" + GetChecksum.hash(url.toASCIIString()) + ".cache");
            fi.delete();
        } catch ( NullPointerException e) { e.printStackTrace(); }

    }
}