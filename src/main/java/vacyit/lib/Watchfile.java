package vacyit.lib;
import java.io.File;
import java.util.*;
import java.net.URI;

public class Watchfile {
    private URI path;

    public void setPath(URI path){
        this.path = path;
    }

    public URI getPath(){
        return this.path;
    }

    private CharSequence findDelimiter(String line){
        List<CharSequence> cs = Arrays.asList(",", "€", "^", "°");
        Iterator<CharSequence> it = cs.iterator();
        while (it.hasNext()) {
            CharSequence c = it.next();
            if (!line.contains(c)) {
                //System.out.println("elected delimiter " + c);
                return c;
            } else {
                if (!it.hasNext()) {
                    System.out.println("exitting: did not find a delimiter that isnt used in " + cs.toString());
                    System.exit(1);
                }
            }
        }
        return "-";
    }

    public LinkedList<Watchitem> read(){
        LinkedList<Watchitem> watchlist = new LinkedList<>();
        try{
            File fi = new File(this.path.getPath());
            Scanner sc = new Scanner(fi);

            while(sc.hasNext()){
                // go in fileloop as long as content is left and pull a line
                String line = sc.next();

                Watchitem watchitem = new Watchitem();
                watchitem.setOperation(line.charAt(0));

                String operators = "=-#+";
                if (!operators.contains(watchitem.getOperation().toString())) {
                    // we have found a new URL to be watched, a delimiter wasnt given as indicated by omitted operator
                    // we are trying to find a delimiter from our list, checking if it is content of the URL
                    watchitem.setDelimiter(findDelimiter(line).charAt(0));
                    line = "+" + watchitem.getDelimiter() + line;
                }

                // we have an operator and expect delimiter on 2nd char in line
                watchitem.setDelimiter(line.charAt(1));
                String[] column = line.split(watchitem.getDelimiter().toString());

                if(column[0].length() != 1){
                    System.out.println("Unexpected operator length");
                    System.exit(1);
                }
                watchitem.setOperation(column[0].charAt(0));

                switch (watchitem.getOperation()) {
                    case '+' -> watchitem.setURL(column[1]);
                    case '=' -> {
                        watchitem.setBoundaries("upper", Double.parseDouble(column[1]));
                        watchitem.setURL(column[2]);
                        watchitem.setCache(Cache.readStream(watchitem.getURL()));
                    }
                    case '-' -> watchitem.setURL(column[2]);
                    case '#' -> watchitem.setURL(column[2]);
                }
                watchlist.add(watchitem);
            }

            sc.close();
        } catch( Exception e){
            e.printStackTrace();
        }

        return watchlist;
    }

    public void write(LinkedList<Watchitem> watchlist){
        try {
            Formatter fo = new Formatter(this.path.getPath());
            for(Watchitem wi: watchlist){
                fo.format("%s"+ wi.getDelimiter() + "%s" + wi.getDelimiter() + "%s%s", wi.getOperation(), wi.getBoundaries("upper"), wi.getURL(), "\n");
            }
            fo.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
