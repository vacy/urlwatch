package vacyit.lib;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.net.http.HttpClient;
import java.net.URI;

public class Webrequest {
    public static String get(URI uri) {
        String body = "";
        try{
            HttpRequest request = HttpRequest.newBuilder()
                                .uri(uri).build();
            HttpResponse<String> response = HttpClient.newHttpClient()
                                .send(request, BodyHandlers.ofString());
            body = response.body();
        }
        catch(java.net.ConnectException e){
            e.printStackTrace();
            System.out.println("Do you have Internet?");
            System.exit(1);
            
        }
        catch(Exception e){ e.printStackTrace(); }

        return body;
    }
}
