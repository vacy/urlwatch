package vacyit.lib;

import org.apache.commons.text.similarity.JaroWinklerSimilarity;

import java.util.zip.CRC32;
import java.util.zip.Checksum;

public class  GetChecksum{
    public static String hash(String webresponse) {
        byte[] bytes = webresponse.getBytes();
        Checksum crc32 = new CRC32();
        crc32.update(bytes, 0, bytes.length);
        return Long.toString(crc32.getValue());
    }

    public static double similarity(String content, String cache) {
        JaroWinklerSimilarity cmpSimilarity = new JaroWinklerSimilarity();
        Double similarity = cmpSimilarity.apply(content, cache);
        return similarity;
    }
}
