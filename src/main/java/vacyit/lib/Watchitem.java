package vacyit.lib;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Watchitem {
    private Character operation;
    private URI url;
    private String hash;
    private Character delimiter;
    private String cache;
    private Map<String, Double> boundaries = new HashMap<>();

    public Watchitem(){
        boundaries.put("lower", 0.100);
        boundaries.put("upper", 0.900);
    }

    public Double getBoundaries(String arg){
        return this.boundaries.get(arg);
    }

    public void setBoundaries(String arg, Double threshold){
        this.boundaries.put(arg, threshold);
    }

    public Character getOperation(){
        return this.operation;
    }

    public void setOperation(Character operation){
        this.operation = operation;
    }

    public Character getDelimiter(){
        return this.delimiter;
    }

    public void setDelimiter(Character delimiter){
        this.delimiter = delimiter;
    }

    public URI getURL(){
        return this.url;
    }

    public void setURL(String uri){
        try {
            this.url = new URI(uri);
        }
        catch(URISyntaxException e){
            e.printStackTrace();
            System.out.println(uri);
        }
    }

    public String getHash(){
        return this.hash;
    }

    public void setHash(String hash){
        this.hash = hash;
    }

    public void setCache(String cache){ this.cache = cache; }
    public String getCache(){ return this.cache; }

}