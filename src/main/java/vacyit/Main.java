package vacyit;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;

import com.google.common.base.Stopwatch;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.text.similarity.JaroWinklerSimilarity;
import vacyit.lib.*;

//import java.util.List;

class Main {

    public static void main(String[] args) {
        // set path of watchfile, take default or take it from call arguments
        Watchfile watchfile = new Watchfile();

        try {
            watchfile.setPath(new URI("/home/jfh/url.watch"));
            if(args.length > 0){ watchfile.setPath(new URI(args[0])); }
        }
        catch (URISyntaxException e){ e.printStackTrace(); }

        // initialize + read our inventory
        LinkedList<Watchitem> watchlist = watchfile.read();
        LinkedList<Watchitem> purgelist = new LinkedList<>();
        for(Watchitem wi: watchlist) {
            // skip and delete routine
            switch (wi.getOperation()) {
                case '-' -> {
                    // it was decided to delete the watchitem, we are looping through purgelist at the end
                    purgelist.add(wi);
                    System.out.println(wi.getURL().toASCIIString() + " deleting");
                    Cache.delete(wi.getURL());
                    continue;
                }

                case '#' -> {
                    System.out.println(wi.getURL() + " is considered commented, ignoring");
                    continue;
                }
            }

            // pull websitecontent
            String content = Webrequest.get(wi.getURL());

            switch (wi.getOperation()) {
                case '+' -> {
                    wi.setCache(content);
                    Cache.writeStream(wi);
                    wi.setOperation('=');
                }
                case '=' -> {
                    System.out.println(wi.getCache().length() + " " + content.length() + " going to check for Similarity on url: " + wi.getURL());
                    Stopwatch timer = Stopwatch.createStarted();
                    Double similarity = GetChecksum.similarity(wi.getCache(), content);
                    System.out.println(similarity + " checked " + timer.stop() + " for Similarity on url: " + wi.getURL());

                    // check if the website has changed
                    // if the Double similarity value is below threshold, then consider the website to be broken
                    // if the Double similarity value is above threshold, then its considered not to have changed
                    if (wi.getBoundaries("lower") < similarity) {
                        wi.setCache(content);
                        Cache.writeStream(wi);
                        if (similarity < wi.getBoundaries("upper")) {
                            System.out.println("website " + wi.getURL() + " changed: " + similarity);
                        }
                    }
                }
            }
        }
        for(Watchitem wi: purgelist){
            //cleanup thee watchlist
            watchlist.remove(wi);
        }

        // we pulled website content+built hashs, removed old lines and so on
        // in other words, work is done, we need to save
        watchfile.write(watchlist);
    }
}