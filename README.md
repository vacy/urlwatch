# UrlWatch



## Getting started
This tool is intended to notify you about changes on websites.
We just put the update onto stdout, its up to you to forward that output to its destination.
The inventory comes from a csv file, we attach one for an example.
Just call the file like this:
./urlwatcher /path/to/urlwatch.file

## How was this implemented?
We pull the websites Index and cache it.
On the next run we pull the websites Index and compare it to the cache. Also write the cache with the new content.

### Comparing
We do a String comparisation, if a threshold is reached you'll be notified. 
You can define the upper threshold in the CSV file.
The lower limit threshold is hardcoded since we expect a malformed reply from the webserver since we dont expect the whole website to change. 
---> This is no monitoring of websites! It is checking if your content has changed.

### Cache
We calculate a CRC32 hash of your URL, then the cache is saved in /tmp with the hashcode of the URL you defined.

## Workflow
Your CSV defines your inventory.
If you want to add a website, you just add the URL to a new line.
When the tool goes through the CSV we're pulling the websites content and rewrite the CSV.
Also it elects the lines delimiter on its own, we have a class of 4 characters that might make up the delimiter, depending on your URL -> we need to find one that is not part of your URL.
=,0.9,https://google.com <- = indicates a known inventory URL, 0.9 is the upper threshold, if your similarity drops below 0.9 you get notified about a change. Values valid between 0 ~ 1
-,0.9,https://google.com <- entry and cache will be removed on the next run
#,0.9,https://google.com <- is ignored / commented

## Limitation
- This is comparing whole website outputs, if you got a huge table to be compared and only want to check a single line,
then you'll also be notified by changes on other lines in the website. You might want to get assisted by a webscraper then.
We have a PHP backend scraper in the works to add the URL to the scraper and fetch dedicated HTML nodes there to be compared by this tool.

